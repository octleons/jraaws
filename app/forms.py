from flask_wtf import FlaskForm
from wtforms import TextAreaField

class TemplateForm(FlaskForm):
    jinja_template = TextAreaField('Text')
    jinja_context = TextAreaField('Text')
