# Jinja for Network Engineers

[TOC]

## Looping

*Template:*
```{% for vlan in range(1, 3) %}
interface ge-0/0.{{ vlan }}
{% endfor %}
```

*Output:*
```interface ge-0/0.1
interface ge-0/0.2
```

**range()** generates an array of numbers that is assigned to a variable in the for loop (**vlan** in the example above). **range()** accepts up to 3 input values:

```
input1: starting value
input2: ending value - 1
input3 (optional): increment value, defaults to 1
```

The for loop above loops from **vlan=1** to **vlan=2**. Note that it stops at 2 even though we passed in 3. The last iteration is ALWAYS 1 lower than the end number defined in the range.

**range()** by default increments the value by 1. To change the increment, add a third input to range:

```
{% for vlan in range(10, 20, 2) %}
interface ge-0/0.{{ vlan }}
{% endfor %}
```

*Output:*
```interface ge-0/0.10
interface ge-0/0.12
interface ge-0/0.14
interface ge-0/0.16
interface ge-0/0.18
```

## Incrementing IP addresses
### IPv4
To increment an IP address from 1 to n, just use the for loop as shown before.

*Template:*
```{% for i in range(1, 4) %}
ip address 10.0.0.{{ i }}/24
{% endfor %}
```

*Output:*
```ip address 10.0.0.1/24
ip address 10.0.0.2/24
ip address 10.0.0.3/24
```

The example above starts counting from 1. A good way to start counting from another number is to introduce an offset.
Here are 2 methods to do it:

#### Define offset inline
There are 2 ways to define offsets within the template. The first method is adding the offset directly to the loop variable

*Template:*
```{% for i in range(1, 4) %}
ip address 10.0.0.{{ i + 10 }}/24
{% endfor %}
```

*Output:*
```ip address 10.0.0.11/24
ip address 10.0.0.12/24
ip address 10.0.0.13/24
```

The second method involves defining a variable in the for loop, and then adding the offset variable to the loop
variable.

*Template:*
```{% for i in range(1, 4) %}
{% set offset=10 %}
ip address 10.0.0.{{ i + offset }}/24
{% endfor %}
```

*Output:*
```ip address 10.0.0.11/24
ip address 10.0.0.12/24
ip address 10.0.0.13/24
```

This method of defining inline offsets is more dynamic as the offset variable can be changed on the fly in the loop.

#### Define offset in context
A second way of introducing offset is by defining a constant variable in the Context section.

*Template:*
```{% for i in range(1, 4) %}
ip address 10.0.0.{{ i + offset }}/24
{% endfor %}
```

*Context:*
```offset=10
```

*Output:*
```ip address 10.0.0.11/24
ip address 10.0.0.12/24
ip address 10.0.0.13/24
```

This can be useful for generating base config for multiple devices, each with different base variables such as hostname
and management IP. This way, only a single template has to be maintained, and each device will have its own context
variables.

#### Octet overflow
In cases where we need more IP addresses than a single octet can hold (0-255), we can perform some division and modulo
operation to split the IP addresses over 2 octets:

*Template:*
```{% for i in range(1, 1000) %}
ip address 10.0.{{ i // 256 }}.{{ i % 256 }}/24
{% endfor %}
```

*Output (extracted):*
```ip address 10.0.0.1/24
...
ip address 10.0.0.254/24
ip address 10.0.0.255/24
ip address 10.0.1.0/24
ip address 10.0.1.1/24
...
```

##### With offset
*Template:*
```{% for i in range(1, 1000) %}
{% set new_i = i + some_offset %}
ip address 10.0.{{ new_i  // 256 }}.{{ new_i % 256 }}/24
{% endfor %}
```

*Context:*
```some_offset=20
```

*Output (extracted):*
```ip address 10.0.0.21/24
...
ip address 10.0.0.254/24
ip address 10.0.0.255/24
ip address 10.0.1.0/24
ip address 10.0.1.1/24
```

**%** is the modulus operator, which basically gives the remainder of the division. **//** is an integer division which
drops the remainder.

### IPv6
IPv6 address gets a bit trickier with its hexadecimal expression. We need to add an additional line to convert the
decimal number to hexadecimal.

*Template:*
```{% for i in range(9, 12) %}
{% set i_hexdec = "{:x}".format(i) %}
ipv6 address 2001::1:{{ i_hexdec}}/64
{% endfor %}
```

*Output:*
```ipv6 address 2001::1:9/64
ipv6 address 2001::1:a/64
ipv6 address 2001::1:b/64
```

#### Group overflow
If we require more addresses than a group of 16 bits can hold in IPv6, we can use division and modulo operations to
utilize 2 or more groups:

*Template:*
```{% for i in range(65534, 65538) %}
{% set i_hexdec_g1 = "{:x}".format(i // 65536 ) %}
{% set i_hexdec_g2 = "{:x}".format(i % 65536 ) %}
ipv6 address 2001::{{ i_hexdec_g1 }}:{{ i_hexdec_g2 }}/64
{% endfor %}
```

*Output:*
```ipv6 address 2001::0:fffe/64
ipv6 address 2001::0:ffff/64
ipv6 address 2001::1:0/64
ipv6 address 2001::1:1/64
```

##### With offset
*Template:*
```{% for i in range(65529, 65534) %}
{% set i_hexdec_g1 = "{:x}".format((i + some_offset) // 65536 ) %}
{% set i_hexdec_g2 = "{:x}".format((i + some_offset) % 65536 ) %}
ipv6 address 2001::{{ i_hexdec_g1 }}:{{ i_hexdec_g2 }}/64
{% endfor %}
```

*Context:*
```some_offset=5
```

*Output:*
```ipv6 address 2001::0:fffe/64
ipv6 address 2001::0:ffff/64
ipv6 address 2001::1:0/64
ipv6 address 2001::1:1/64
ipv6 address 2001::1:2/64
```

## Adding prefixes in description
Sometimes we need a number is represented as a string with a set number of characters, e.g. representing
subinterface number in the customer description with the format **NxxxxxxxR**, where x is the subinterface number
prepended with 0s. If we just place the loop variable in between **N** and **R**:

*Template:*
```{% for i in range(1, 3) %}
interface ge-0/1.{{ i }}
  description customer_N{{ i }}R
{% endfor %}
```

*Output:*
```interface ge-0/1.1
  description customer_N1R
interface ge-0/1.2
  description customer_N2R
```

Notice that we are missing the prefix. An additional change is required to prepend the subinterface number with the
right number of 0s to make up 7 digits:

*Template:*
```{% for i in range(1, 3) %}
interface ge-0/1.{{ i }}
  description customer_N{{ '%07d' % (i) }}R
{% endfor %}
```

*Output:*
```interface ge-0/1.1
  description customer_N0000001R
interface ge-0/1.2
  description customer_N0000002R
```

**N{{ i }}R** has been replaced with **N{{ '%07d' % (i) }}R**. The **0** is the character to prepend the digit **i** and **7** is
total length of the string.