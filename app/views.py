from __future__ import print_function
import re

import os

from app import app
from flask import render_template, flash, redirect
import markdown
from .forms import TemplateForm

from jinja2 import Template
from jinja2.exceptions import TemplateSyntaxError, UndefinedError

@app.route('/')
@app.route('/index')
def index():
    user = {'nickname': 'Leon'}
    return render_template('index.html', title="Home", user=user)

@app.route('/render', methods=["GET", "POST"])
def render():
    form = TemplateForm()
    output = ""
    if form.validate_on_submit():
        context_list = str(form.jinja_context.data).splitlines()
        context = {}
        for context_entry in context_list:
            matches = re.search("(?m)^\s*(\w+)\s*[=:]\s*(.+?)\s*$", context_entry)
            variable, value = matches.groups()
            try:
                value = int(value)
            except TypeError:
                pass

            context[variable] = value

        try:
            template = Template(form.jinja_template.data, trim_blocks=True)
            output = template.render(context)
        except (TemplateSyntaxError, UndefinedError) as err_msg:
            flash(err_msg)
            output = ""

    return render_template("render_jinja.html", title="Render Jinja", form=form, output=output)

@app.route('/render/help', methods=["GET"])
def show_jraaws_help():
    script_dir = os.path.dirname(__file__)
    with open(os.path.join(script_dir, "static/jinja_for_network_engineers.md"), "r") as f:
        help_markdown = f.read()

    help_html = markdown.markdown(help_markdown, extensions=["markdown.extensions.toc"])
    return render_template("jinja_help.html", title="Jinja For Network Engineers", help_block=help_html)